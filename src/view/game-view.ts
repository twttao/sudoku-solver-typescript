import { Game } from '../model/Game';

export class GameView {
    public game : Game;
    public solutions : Set<string>;

    bind() {
        this.game = new Game();
        this.solutions = new Set();
    }

    automaticSolve(game) {
        this.autoSolve(game).then(() => {
            console.log(`Found ${this.solutions.size} solutions to this game.`)
            console.log(this.solutions.values())
        });
    }

    autoSolve(game) : Promise<any> {
        return Promise.all(game.cells
            .filter(cell => !cell.isResolved())
            .map(cell => {
                return Promise.all(Array.from(cell.states).map(state => {
                    return cell.simulateSolve(state).then(gameCopy => {

                        if(!gameCopy.isValid()) {
                            return;
                        }

                        if(gameCopy.isResolved()) {
                            this.solutions.add(gameCopy.toSolution());
                            return;
                        }

                        return this.autoSolve(gameCopy);
                    });
                }));
            })
        );
    }

    exampleGame() {
        this.game._getBlock(1, 1).cells[0].resolve(1);
        this.game._getBlock(1, 1).cells[3].resolve(7);
        this.game._getBlock(1, 1).cells[4].resolve(3);
        this.game._getBlock(1, 1).cells[5].resolve(9);
        this.game._getBlock(1, 1).cells[6].resolve(4);
        this.game._getBlock(1, 1).cells[7].resolve(6);
        this.game._getBlock(1, 1).cells[8].resolve(8);

        this.game._getBlock(1, 2).cells[0].resolve(4);
        this.game._getBlock(1, 2).cells[1].resolve(8);
        this.game._getBlock(1, 2).cells[2].resolve(9);
        this.game._getBlock(1, 2).cells[3].resolve(2);
        this.game._getBlock(1, 2).cells[4].resolve(5);
        this.game._getBlock(1, 2).cells[5].resolve(6);
        this.game._getBlock(1, 2).cells[6].resolve(3);
        this.game._getBlock(1, 2).cells[7].resolve(7);
        this.game._getBlock(1, 2).cells[8].resolve(1);

        this.game._getBlock(1, 3).cells[0].resolve(3);
        this.game._getBlock(1, 3).cells[1].resolve(7);
        this.game._getBlock(1, 3).cells[2].resolve(6);
        this.game._getBlock(1, 3).cells[3].resolve(8);
        this.game._getBlock(1, 3).cells[4].resolve(4);
        this.game._getBlock(1, 3).cells[5].resolve(1);
        this.game._getBlock(1, 3).cells[6].resolve(2);
        this.game._getBlock(1, 3).cells[7].resolve(9);
        this.game._getBlock(1, 3).cells[8].resolve(5);

        this.game._getBlock(2, 1).cells[0].resolve(3);
        this.game._getBlock(2, 1).cells[1].resolve(8);
        this.game._getBlock(2, 1).cells[2].resolve(7);
        this.game._getBlock(2, 1).cells[3].resolve(5);
        this.game._getBlock(2, 1).cells[4].resolve(9);
        this.game._getBlock(2, 1).cells[8].resolve(6);

        this.game._getBlock(2, 2).cells[0].resolve(1);
        this.game._getBlock(2, 2).cells[1].resolve(2);
        this.game._getBlock(2, 2).cells[2].resolve(4);
        this.game._getBlock(2, 2).cells[3].resolve(7);
        this.game._getBlock(2, 2).cells[4].resolve(6);
        this.game._getBlock(2, 2).cells[5].resolve(3);
        this.game._getBlock(2, 2).cells[6].resolve(8);
        this.game._getBlock(2, 2).cells[7].resolve(9);
        this.game._getBlock(2, 2).cells[8].resolve(5);

        this.game._getBlock(2, 3).cells[0].resolve(6);
        this.game._getBlock(2, 3).cells[1].resolve(5);
        this.game._getBlock(2, 3).cells[2].resolve(9);
        this.game._getBlock(2, 3).cells[3].resolve(4);
        this.game._getBlock(2, 3).cells[5].resolve(8);
        this.game._getBlock(2, 3).cells[6].resolve(7);
        this.game._getBlock(2, 3).cells[8].resolve(3);

        this.game._getBlock(3, 1).cells[0].resolve(9);
        this.game._getBlock(3, 1).cells[6].resolve(8);
        this.game._getBlock(3, 1).cells[7].resolve(7);
        this.game._getBlock(3, 1).cells[8].resolve(3);

        this.game._getBlock(3, 2).cells[0].resolve(6);
        this.game._getBlock(3, 2).cells[1].resolve(3);
        this.game._getBlock(3, 2).cells[2].resolve(7);
        this.game._getBlock(3, 2).cells[3].resolve(9);
        this.game._getBlock(3, 2).cells[4].resolve(4);
        this.game._getBlock(3, 2).cells[5].resolve(8);
        this.game._getBlock(3, 2).cells[6].resolve(5);
        this.game._getBlock(3, 2).cells[7].resolve(1);
        this.game._getBlock(3, 2).cells[8].resolve(2);
    }
}