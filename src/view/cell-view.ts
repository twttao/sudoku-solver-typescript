import {BindingEngine, inject, bindable} from 'aurelia-framework';

@inject(BindingEngine)
export class CellView {
    @bindable cell;
    private bindingEngine : BindingEngine;
    public states : Set<number>;

    constructor(bindingEngine) {
        this.bindingEngine = bindingEngine;
    }

    bind() {
        this._flushViewModel();

        this.bindingEngine.propertyObserver(this.cell.states, 'size')
            .subscribe(() => {
                this._flushViewModel();
            });
    }

    _flushViewModel() {
        this.states = new Set(this.cell.getStateSnapshot() as Array<number>);
    }

    resolve(value) {
        this.cell.resolve(value).catch(() => {});
    }
}