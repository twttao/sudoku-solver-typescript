import {bindable} from 'aurelia-framework';

export class BlockView {
    @bindable block;
}