import { SudokuConstraintSet } from './SudokuConstraintSet';

export class Row extends SudokuConstraintSet{
    constructor(rowCells) {
        super(rowCells);

        this.cells.forEach(cell => cell.subscribe(this));
    }
}