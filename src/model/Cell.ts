import { CellResolutionEvent } from './event/type/CellResolutionEvent';
import { CellReductionEvent } from './event/type/CellReductionEvent';
import { CellInvalidateEvent } from './event/type/CellInvalidateEvent';
import { EventPublisher } from './event/EventPublisher';
import {Block} from "./Block";
import {Game} from "./Game";

export class Cell extends EventPublisher{
    public states : Set<number>;
    private _isResolved : boolean;
    private _block : Block;
    private _x : number;
    private _y : number;

    constructor(block:Block = null, x:number = null, y:number = null) {
        super();

        this.states = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        this._isResolved = false;

        this._block = block;
        this._x = x;
        this._y = y;
    }

    public isResolved() : boolean {
        return this._isResolved;
    }

    public resolve(value : number) : Promise<any>{
        if(this.isResolved()) {
            return;
        }

        if(!this.hasState(value)) {
            throw new Error('Cell does not contain value. Therefore cannot resolve this value.');
        }

        for(let state of this.states) {
            if(state !== value) {
                this.states.delete(state);
            }
        }

        this._isResolved = true;
        return this.publishEvents(CellResolutionEvent, value);
    }

    public reduce(value : number) : Promise<any> {
        if(!this.hasState(value)) {
            return;
        }

        if(this.isResolved() && this.hasState(value)) {
            return this.publishEvents(CellInvalidateEvent, null);
        }

        this.states.delete(value);

        return Promise.all([
            this.stateSize() === 1 ? this.resolve(this.firstState()) : null,
            this.publishEvents(CellReductionEvent, value)
        ]);
    }

    public clone(block:Block = null, x:number = null, y:number = null) : Cell {
        var clonedCell = new Cell(block, x, y);
        clonedCell.states = new Set(this.getStateSnapshot());
        clonedCell._isResolved = this._isResolved;

        return clonedCell;
    }

    public simulateSolve(value) : Promise<Game> {
        var gameCopy = this._block._game.clone();
        var cellPosition = this.positionInGame();

        return gameCopy.cells[cellPosition].resolve(value)
            .then(() => gameCopy).catch(() => gameCopy);
    };

    private positionInBlock() : number {
        return (this._x - 1) * 3 + (this._y - 1);
    }

    public positionInGame() : number {
        return this._block.positionInGame() * 9 + this.positionInBlock();
    }

    public toSolution() : string {
        return '' + this.firstState();
    }

    public firstState() : number {
        return this.states.values().next().value;
    }

    public stateSize() : number {
        return this.states.size;
    }

    public hasState(value) : boolean {
        return this.states.has(value);
    }

    public getStateSnapshot() : Array<number> {
        return Array.from(this.states);
    }
}