import {AbstractEventConsumer} from "./AbstractEventConsumer";
export class EventPublisher {
    private _consumers : Set<AbstractEventConsumer>;

    constructor() {
        this._consumers = new Set();
    }

    subscribe(consumer : AbstractEventConsumer) {
        this._consumers.add(consumer);
    }

    publishEvents(eventName, value : any) : Promise<any> {
        var eventConsumers = Array.from(this._consumers);
        return Promise.all(eventConsumers.map(consumer => consumer.feedEvent(eventName, value, this)));
    }
}