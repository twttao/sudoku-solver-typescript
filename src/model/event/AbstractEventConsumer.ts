import { Event } from './type/Event';

export abstract class AbstractEventConsumer {
    public _events : Array<Event>;

    constructor() {
        this._events = [];

        setTimeout(this._consumeEvent.bind(this), 0);
    }

    abstract _onEventConsume(event: Event) : Promise<any>;

    public feedEvent(eventName, value : any, target : any) : Promise<any> {
        var event = new eventName(value, target);
        this._events.push(event);
        return event.callbackPromise;
    }


    public _getEvents() : Array<Event> {
        return this._events;
    }

    private _consumeEvent() {
        if(this._events.length > 0) {
            var event = this._events.shift();

            this._onEventConsume(event).then(() => {
                if(event.isRejection) {
                    event.reject();
                } else {
                    event.callback();
                }
            }).catch(() => {
                event.reject();
            });
        }

        setTimeout(this._consumeEvent.bind(this), 0);
    }
}