export class Event {
    public callbackPromise : Promise<any>;
    public callback : Function;
    public reject : Function;
    public value : any;
    public target: any;
    public isRejection: boolean;

    constructor(value, target) {
        this.callbackPromise = new Promise((resolve, reject) => {
            this.callback = resolve;
            this.reject = reject;
        });
        this.value = value;
        this.target = target;

        this.isRejection = false;
    }
}