import { Event } from './Event';

export class CellInvalidateEvent extends Event{
    constructor () {
        super(null, null);

        this.isRejection = true;
    }
}