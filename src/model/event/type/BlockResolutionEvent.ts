import { Event } from './Event';

export class BlockResolutionEvent extends Event{
    constructor(value, target) {
        super(value, target);
    }
}