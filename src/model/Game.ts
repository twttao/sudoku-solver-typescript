import { Block } from './Block';
import { Column } from './Column';
import { Row } from './Row';
import {Cell} from "./Cell";
import {Event} from "./event/type/Event";
import {BlockResolutionEvent} from "./event/type/BlockResolutionEvent";
import {BlockInvalidateEvent} from "./event/type/BlockInvalidateEvent";
import {AbstractEventConsumer} from "./event/AbstractEventConsumer";

export class Game extends AbstractEventConsumer {
    public blocks : Array<Block>;
    public rows : Array<Row>;
    public columns : Array<Column>;
    public cells : Array<Cell>;

    private _isResolved: boolean;
    private _isValid : boolean;

    private blockResolutionEventHistoryCache : Set<Block>;
    private blockInvalidateEventHistoryCache : boolean;

    constructor(blocks:Array<Block> = null) {
        super();

        this.blocks = blocks ? blocks : [
            new Block(this, 1, 1), new Block(this, 1, 2), new Block(this, 1, 3),
            new Block(this, 2, 1), new Block(this, 2, 2), new Block(this, 2, 3),
            new Block(this, 3, 1), new Block(this, 3, 2), new Block(this, 3, 3)
        ];

        this.rows = [
            this._getRow(1),
            this._getRow(2),
            this._getRow(3),
            this._getRow(4),
            this._getRow(5),
            this._getRow(6),
            this._getRow(7),
            this._getRow(8),
            this._getRow(9),
        ];

        this.columns = [
            this._getColumn(1),
            this._getColumn(2),
            this._getColumn(3),
            this._getColumn(4),
            this._getColumn(5),
            this._getColumn(6),
            this._getColumn(7),
            this._getColumn(8),
            this._getColumn(9)
        ];

        this.cells = [].concat.apply([], this.blocks.map(block => block.cells));

        this._isResolved = false;
        this._isValid = true;

        this.blockResolutionEventHistoryCache  = new Set();
        this.blockInvalidateEventHistoryCache = false;
    }

    public isResolved() : boolean {
        return this._isResolved;
    }

    public resolve() : Promise<any> {
        this._isResolved = true;

        return null;
    }

    public isValid() : boolean {
        return this._isValid;
    }

    public invalidate() : Promise<any> {
        this._isValid = false;

        return null;
    }

    public clone() : Game {
        var clonedBlocks = this.blocks.map((block, index) => {
            var blockX = Math.ceil((index + 1) / 3);
            var blockY = index % 3 + 1;

            return block.clone(null, blockX, blockY);
        });

        var clonedGame = new Game(clonedBlocks);

        clonedGame.blocks.forEach(block => block._game = clonedGame);

        clonedGame._isResolved = this._isResolved;
        clonedGame._isValid = this._isValid;
        return clonedGame;
    }

    public toSolution() : string{
        if(!this.isResolved()) {
            throw new Error('Cannot get solution of an unresolved game!');
        }

        return this.cells
            .map(cell => cell.toSolution())
            .reduce((previous, current) => previous + current);
    }

    _onEventConsume(event: Event) : Promise<any> {
        if(event instanceof BlockResolutionEvent && this.blockResolutionEventHistoryCache.has(event.target)) {
            return null;
        }
        if(event instanceof BlockInvalidateEvent && this.blockInvalidateEventHistoryCache) {
            return null;
        }

        if(event instanceof BlockResolutionEvent) {
            this.blockResolutionEventHistoryCache.add(event.target);
        }
        if(event instanceof BlockInvalidateEvent) {
            this.blockInvalidateEventHistoryCache = true;
        }

        var areAllBlocksResolved = this.blocks.every(block => block.isResolved());

        return Promise.all([
            event instanceof BlockResolutionEvent && areAllBlocksResolved ? this.resolve() : null,
            event instanceof BlockInvalidateEvent  ? this.invalidate() : null
        ]);
    }

    public _getBlock(row, column) : Block {
        var index = 3 * (row - 1) + (column - 1);
        return this.blocks[index];
    }

    public _getRow(rowNum) : Row{
        var blockNum = Math.ceil(rowNum / 3);
        var blockRowNum = (rowNum - 1) % 3 + 1;

        var rowCells = [...this._getBlock(blockNum, 1)._getRow(blockRowNum), ...this._getBlock(blockNum, 2)._getRow(blockRowNum), ...this._getBlock(blockNum, 3)._getRow(blockRowNum)];
        return new Row(rowCells);
    }

    public _getColumn(columnNum) : Column {
        var blockNum = Math.ceil(columnNum / 3);
        var blockColumnNum = (columnNum - 1) % 3 + 1;

        var columnCells = [
            ...this._getBlock(1, blockNum)._getColumn(blockColumnNum),
            ...this._getBlock(2, blockNum)._getColumn(blockColumnNum),
            ...this._getBlock(3, blockNum)._getColumn(blockColumnNum)
        ];
        return new Column(columnCells);
    }
}