import { Event } from './event/type/Event';
import {Cell} from "./Cell";
import {CellResolutionEvent} from "./event/type/CellResolutionEvent";
import {CellReductionEvent} from "./event/type/CellReductionEvent";
import {CellInvalidateEvent} from "./event/type/CellInvalidateEvent";
import {Block} from "./Block";
import {AbstractEventConsumer} from "./event/AbstractEventConsumer";

export class SudokuConstraintSet extends AbstractEventConsumer{
    public cells : Array<Cell>;
    private cellResolutionEventHistoryCache : Set<Cell>;
    private cellInvalidateEventHistoryCache : boolean;
    private cellReductionEventHistoryCache : Map<Cell, Set<number>>;

    constructor(cells:Array<Cell> = null) {
        super();

        this.cells = cells;
        this.cellResolutionEventHistoryCache = new Set();
        this.cellInvalidateEventHistoryCache = false;
        this.cellReductionEventHistoryCache = new Map();
    }

    public reduceAllCellsExcept(value : number, target : Cell) : Promise<any> {
        return Promise.all(this.cells.map(cell => {
                if(cell === target) {
                    return;
                }

                return cell.reduce(value);
            }));
    }

    public inspectStateUniqueness(value : number) : Promise<any>{
        var cellsWithState = this.cells.filter(cell => {
            return cell.hasState(value);
        });

        return cellsWithState.length === 1 ? cellsWithState[0].resolve(value) : null;
    }

    _onEventConsume(event: Event) : Promise<any> {
        if(event instanceof CellResolutionEvent && this.cellResolutionEventHistoryCache.has(event.target)) {
            return null;
        }
        if(event instanceof CellInvalidateEvent && this.cellInvalidateEventHistoryCache) {
            return null;
        }
        if(event instanceof CellReductionEvent && this.cellReductionEventHistoryCache.get(event.target) &&
                                                  this.cellReductionEventHistoryCache.get(event.target).has(event.value)) {
            return null;
        }


        if(event instanceof CellResolutionEvent){
            this.cellResolutionEventHistoryCache.add(event.target);
        }
        if(event instanceof CellInvalidateEvent) {
            this.cellInvalidateEventHistoryCache = true;
        }
        if(event instanceof CellReductionEvent) {
            if(!this.cellReductionEventHistoryCache.get(event.target)) {
                this.cellReductionEventHistoryCache.set(event.target, new Set([event.value]));
            } else {
                this.cellReductionEventHistoryCache.get(event.target).add(event.value);
            }
        }

        var areAllCellsResolved = this.cells.every(cell => cell.isResolved());

        return Promise.all([
            event instanceof CellResolutionEvent ? this.reduceAllCellsExcept(event.value, event.target) : null,
            event instanceof CellReductionEvent ? this.inspectStateUniqueness(event.value) : null,
            event instanceof CellResolutionEvent && this instanceof Block && areAllCellsResolved ? (this as Block).resolve() : null,
            event instanceof CellInvalidateEvent && this instanceof Block ? (this as Block).invalidate() : null
        ]);
    }
}