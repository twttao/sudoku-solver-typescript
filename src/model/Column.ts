import { SudokuConstraintSet } from './SudokuConstraintSet';

export class Column extends SudokuConstraintSet{
    constructor(columnCells) {
        super(columnCells);

        this.cells.forEach(cell => cell.subscribe(this));
    }
}