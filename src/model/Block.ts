import { SudokuConstraintSet } from './SudokuConstraintSet';
import { Cell } from './Cell';
import { BlockInvalidateEvent } from './event/type/BlockInvalidateEvent';
import { BlockResolutionEvent } from './event/type/BlockResolutionEvent';
import {Game} from "./Game";

export class Block extends SudokuConstraintSet{
    public _game : Game;
    public _isResolved : boolean;
    private _isValid : boolean;
    private _x : number;
    private _y : number;

    constructor(game:Game = null, x:number = null, y:number = null, cells:Array<Cell> = null) {
        super();

        this.cells = cells ? cells : [
            new Cell(this, 1, 1), new Cell(this, 1, 2), new Cell(this, 1, 3),
            new Cell(this, 2, 1), new Cell(this, 2, 2), new Cell(this, 2, 3),
            new Cell(this, 3, 1), new Cell(this, 3, 2), new Cell(this, 3, 3)
        ];

        this.cells.forEach(cell => cell.subscribe(this));

        this._game = game;
        this._x = x;
        this._y = y;

        this._isResolved = false;
        this._isValid = true;
    }

    public resolve() : Promise<any> {
        if(this.isResolved()) {
            return;
        }

        this._isResolved = true;

        return this._game && this._game.feedEvent(BlockResolutionEvent, null, this);
    }

    public isResolved() : boolean {
        return this._isResolved;
    }

    public invalidate() : Promise<any> {
        if(!this.isValid()) {
            return;
        }

        this._isValid = false;

        return this._game && this._game.feedEvent(BlockInvalidateEvent, null, null);
    }

    public isValid() : boolean{
        return this._isValid;
    }

    public clone(game:Game = null, x:number = null, y:number = null) : Block {
        var clonedCells = this.cells.map((cell, index) => {
            var cellX = Math.ceil((index + 1) / 3);
            var cellY = index % 3 + 1;

            return cell.clone(this, cellX, cellY);
        });

        var clonedBlock = new Block(game, x, y, clonedCells);
        clonedBlock._isResolved = this._isResolved;
        clonedBlock._isValid = this._isValid;
        return clonedBlock;
    }

    public positionInGame() {
        return (this._x - 1) * 3 + (this._y - 1);
    }

    public _getRow(number : number) : Array<Cell> {
        switch(number) {
            case 1:
                return [this.cells[0], this.cells[1], this.cells[2]];
            case 2:
                return [this.cells[3], this.cells[4], this.cells[5]];
            case 3:
                return [this.cells[6], this.cells[7], this.cells[8]];
        }
    }

    public _getColumn(number : number) : Array<Cell> {
        switch(number) {
            case 1:
                return [this.cells[0], this.cells[3], this.cells[6]];
            case 2:
                return [this.cells[1], this.cells[4], this.cells[7]];
            case 3:
                return [this.cells[2], this.cells[5], this.cells[8]];
        }
    }
}