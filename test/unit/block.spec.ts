import { Block } from '../../src/model/Block';
import { CellResolutionEvent } from '../../src/model/event/type/CellResolutionEvent';
import { CellReductionEvent } from '../../src/model/event/type/CellReductionEvent';
import { CellInvalidateEvent } from '../../src/model/event/type/CellInvalidateEvent';
import { createCellWith, isSetEqual } from './test-utils';

describe('Block model', () => {

    it('should be initialized with nine new cells', () => {
        var block = new Block();

        expect(block.cells.length).toEqual(9);

        var isEveryCellInitialized = block.cells.every(cell => {
            return isSetEqual(cell.states, new Set([1, 2, 3, 4, 5, 6, 7, 8, 9]));
        });
        expect(isEveryCellInitialized).toBeTruthy();
    });

    it('should be able to get rows', () => {
        var block = new Block();

        var firstRow = block._getRow(1);
        var secondRow = block._getRow(2);
        var thirdRow = block._getRow(3);

        expect(firstRow[0]).toBe(block.cells[0]);
        expect(firstRow[1]).toBe(block.cells[1]);
        expect(firstRow[2]).toBe(block.cells[2]);
        expect(secondRow[0]).toBe(block.cells[3]);
        expect(secondRow[1]).toBe(block.cells[4]);
        expect(secondRow[2]).toBe(block.cells[5]);
        expect(thirdRow[0]).toBe(block.cells[6]);
        expect(thirdRow[1]).toBe(block.cells[7]);
        expect(thirdRow[2]).toBe(block.cells[8]);
    });

    it('should be able to get columns', () => {
        var block = new Block();

        var firstColumn = block._getColumn(1);
        var secondColumn = block._getColumn(2);
        var thirdColumn = block._getColumn(3);

        expect(firstColumn[0]).toBe(block.cells[0]);
        expect(firstColumn[1]).toBe(block.cells[3]);
        expect(firstColumn[2]).toBe(block.cells[6]);
        expect(secondColumn[0]).toBe(block.cells[1]);
        expect(secondColumn[1]).toBe(block.cells[4]);
        expect(secondColumn[2]).toBe(block.cells[7]);
        expect(thirdColumn[0]).toBe(block.cells[2]);
        expect(thirdColumn[1]).toBe(block.cells[5]);
        expect(thirdColumn[2]).toBe(block.cells[8]);
    });

    it('should be able to be cloned', (done) => {
        var block = new Block();
        block.cells[0].resolve(2).then(() => {
            var block2 = block.clone();
            expect(block2.cells[0].isResolved()).toBeTruthy();
            expect(block2.cells[0].firstState()).toEqual(2);

            block2.cells[1].resolve(6).then(() => {
                expect(block2.cells[1].isResolved()).toBeTruthy();
                expect(block.cells[1].isResolved()).toBeFalsy();

                done();
            });
        });
    });
});

describe('Resolution event', () => {
    it('should receive cell resolution event after cell is resolved', () => {
        var block = new Block();

        block.cells[1].resolve(6);

        expect(block._getEvents()[0] instanceof CellResolutionEvent).toBeTruthy();
    });

    it('should receive cell reduction event after cell is reduced', () => {
        var block = new Block();

        block.cells[1].reduce(6);

        expect(block._getEvents()[0] instanceof CellReductionEvent).toBeTruthy();
    });

    it('should respond to cell resolution event and mutate states of children blocks', (done) => {
        var block = new Block();

        block.cells[1].resolve(6).then(() => {
            var expectedStates = new Set([1, 2, 3, 4, 5, 7, 8, 9]);
            expect(isSetEqual(block.cells[0].states, expectedStates));
            expect(isSetEqual(block.cells[1].states, new Set([6])));
            expect(isSetEqual(block.cells[2].states, expectedStates));
            expect(isSetEqual(block.cells[3].states, expectedStates));
            expect(isSetEqual(block.cells[4].states, expectedStates));
            expect(isSetEqual(block.cells[5].states, expectedStates));
            expect(isSetEqual(block.cells[6].states, expectedStates));
            expect(isSetEqual(block.cells[7].states, expectedStates));
            expect(isSetEqual(block.cells[8].states, expectedStates));

            done();
        });
    });

    it('should respond to cell reduction event and mutate states of children blocks', (done) => {
        var block = new Block();
        createCellWith(block.cells[0], new Set([9]));
        createCellWith(block.cells[1], new Set([6]));
        createCellWith(block.cells[2], new Set([3, 4]));
        createCellWith(block.cells[3], new Set([1, 4]));
        createCellWith(block.cells[4], new Set([8]));
        createCellWith(block.cells[5], new Set([1, 4, 7]));
        createCellWith(block.cells[6], new Set([2, 5, 7]));
        createCellWith(block.cells[7], new Set([2, 3, 7]));
        createCellWith(block.cells[8], new Set([2, 3, 5, 7]));

        block.cells[5].reduce(1).then(() => {
            expect(isSetEqual(block.cells[0].states, new Set([9])));
            expect(isSetEqual(block.cells[1].states, new Set([6])));
            expect(isSetEqual(block.cells[2].states, new Set([3, 4])));
            expect(isSetEqual(block.cells[3].states, new Set([1])));
            expect(isSetEqual(block.cells[4].states, new Set([8])));
            expect(isSetEqual(block.cells[5].states, new Set([4, 7])));
            expect(isSetEqual(block.cells[6].states, new Set([2, 5, 7])));
            expect(isSetEqual(block.cells[7].states, new Set([2, 3, 7])));
            expect(isSetEqual(block.cells[8].states, new Set([2, 3, 5, 7])));

            done();
        });
    });

    it('should mark block as resolved after last cell resolution event received', (done) => {
        var block = new Block();
        expect(block.isResolved()).toBeFalsy();

        createCellWith(block.cells[0], new Set([9]), true);
        createCellWith(block.cells[1], new Set([6]), true);
        createCellWith(block.cells[2], new Set([4]), true);
        createCellWith(block.cells[3], new Set([1]), true);
        createCellWith(block.cells[4], new Set([8]), true);
        createCellWith(block.cells[5], new Set([7]), true);
        createCellWith(block.cells[6], new Set([2]), true);
        createCellWith(block.cells[7], new Set([3]), true);
        createCellWith(block.cells[8], new Set([3, 5]), false);

        block.cells[8].resolve(5).then(() => {
            expect(block.isResolved()).toBeTruthy();

            done();
        });
    });
});

describe('Invalidate event', () => {
    it('should receive cell invalidate event after detecting attempt to reduce last possible state', () => {
        var block = new Block();
        createCellWith(block.cells[0], new Set([9]), true);
        createCellWith(block.cells[1], new Set([6]), true);
        createCellWith(block.cells[2], new Set([4]), true);
        createCellWith(block.cells[3], new Set([1]), true);
        createCellWith(block.cells[4], new Set([8]), true);
        createCellWith(block.cells[5], new Set([7]), true);
        createCellWith(block.cells[6], new Set([2]), true);
        createCellWith(block.cells[7], new Set([3]), true);
        createCellWith(block.cells[8], new Set([5]), true);

        block.cells[8].reduce(5).catch(() => {});

        expect(block._getEvents()[0] instanceof CellInvalidateEvent).toBeTruthy();
    });

    it('should mark block as invalid after receiving cell invalidate event', (done) => {
        var block = new Block();
        createCellWith(block.cells[0], new Set([9]), true);
        createCellWith(block.cells[1], new Set([6]), true);
        createCellWith(block.cells[2], new Set([4]), true);
        createCellWith(block.cells[3], new Set([1]), true);
        createCellWith(block.cells[4], new Set([8]), true);
        createCellWith(block.cells[5], new Set([7]), true);
        createCellWith(block.cells[6], new Set([2]), true);
        createCellWith(block.cells[7], new Set([3]), true);
        createCellWith(block.cells[8], new Set([5]), true);

        expect(block.isValid()).toBeTruthy();

        block.cells[8].reduce(5).catch(() => {
            expect(block.isValid()).toBeFalsy();

            done();
        });
    });
});