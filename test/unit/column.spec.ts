import { Game } from '../../src/model/Game';
import { Cell } from '../../src/model/Cell';
import { Column } from '../../src/model/Column';
import { CellResolutionEvent } from '../../src/model/event/type/CellResolutionEvent';
import { CellReductionEvent } from '../../src/model/event/type/CellReductionEvent';
import { isSetEqual } from "./test-utils";

describe('Column model', () => {
    it('should receive cell resolution event after cell is resolved', () => {
        var game = new Game();
        var firstCell = game._getBlock(1, 1).cells[0];

        firstCell.resolve(7);

        var columnOne = game.columns[0];
        expect(columnOne._getEvents()[0] instanceof CellResolutionEvent).toBeTruthy();
    });

    it('should receive cell reduction event after cell is reduced', () => {
        var game = new Game();
        var firstCell = game._getBlock(1, 1).cells[0];

        firstCell.reduce(7);

        var columnOne = game.columns[0];
        expect(columnOne._getEvents()[0] instanceof CellReductionEvent).toBeTruthy();
    });

    it('should respond to cell resolution event and mutate states of children blocks', (done) => {
        var game = new Game();
        var firstCell = game._getBlock(1, 1).cells[0];

        var columnOne = game.columns[0];
        var expectedStates = new Set([1, 2, 3, 4, 5, 6, 8, 9]);
        firstCell.resolve(7).then(() => {
            expect(isSetEqual(columnOne.cells[0].states, new Set([7])));
            expect(isSetEqual(columnOne.cells[1].states, expectedStates));
            expect(isSetEqual(columnOne.cells[2].states, expectedStates));
            expect(isSetEqual(columnOne.cells[3].states, expectedStates));
            expect(isSetEqual(columnOne.cells[4].states, expectedStates));
            expect(isSetEqual(columnOne.cells[5].states, expectedStates));
            expect(isSetEqual(columnOne.cells[6].states, expectedStates));
            expect(isSetEqual(columnOne.cells[7].states, expectedStates));
            expect(isSetEqual(columnOne.cells[8].states, expectedStates));

            done();
        });
    });

    it('should respond to cell reduction event and mutate states of children blocks', (done) => {
        var createCellWith = (cell, targetSet) => {
            for(let state of cell.states) {
                if(!targetSet.has(state)) {
                    cell.states.delete(state);
                }
            }
            return cell;
        };

        var column = new Column([
            createCellWith(new Cell(), new Set([9])),
            createCellWith(new Cell(), new Set([6])),
            createCellWith(new Cell(), new Set([3, 4])),
            createCellWith(new Cell(), new Set([1, 4])),
            createCellWith(new Cell(), new Set([8])),
            createCellWith(new Cell(), new Set([1, 4, 7])),
            createCellWith(new Cell(), new Set([2, 5, 7])),
            createCellWith(new Cell(), new Set([2, 3, 7])),
            createCellWith(new Cell(), new Set([2, 3, 5, 7])),
        ]);

        column.cells[5].reduce(1).then(() => {
            expect(isSetEqual(column.cells[0].states, new Set([9])));
            expect(isSetEqual(column.cells[1].states, new Set([6])));
            expect(isSetEqual(column.cells[2].states, new Set([3, 4])));
            expect(isSetEqual(column.cells[3].states, new Set([1])));
            expect(isSetEqual(column.cells[4].states, new Set([8])));
            expect(isSetEqual(column.cells[5].states, new Set([4, 7])));
            expect(isSetEqual(column.cells[6].states, new Set([2, 5, 7])));
            expect(isSetEqual(column.cells[7].states, new Set([2, 3, 7])));
            expect(isSetEqual(column.cells[8].states, new Set([2, 3, 5, 7])));

            done();
        });
    });
});