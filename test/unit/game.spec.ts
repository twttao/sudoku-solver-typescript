import { Game } from '../../src/model/Game';
import {isArrayEqual} from "./test-utils";

describe('Game model', () => {
    it('should be initialized with nine new blocks', () => {
        var game = new Game();

        expect(game.blocks.length).toEqual(9);

        var isEveryBlockInitialized = game.blocks.every(block => {
            return block.cells.length === 9;
        });
        expect(isEveryBlockInitialized).toBeTruthy();
    });

    it('should be able to get block given row and column', () => {
        var game = new Game();

        expect(game._getBlock(1, 1)).toBe(game.blocks[0]);
        expect(game._getBlock(1, 2)).toBe(game.blocks[1]);
        expect(game._getBlock(2, 1)).toBe(game.blocks[3]);
        expect(game._getBlock(2, 2)).toBe(game.blocks[4]);
    });

    it('should be able to get row', () => {
        var game = new Game();

        var actualFirstRow = game._getRow(1).cells;
        var expectedFirstRow = [...game._getBlock(1, 1)._getRow(1), ...game._getBlock(1, 2)._getRow(1), ...game._getBlock(1, 3)._getRow(1)];
        expect(isArrayEqual(actualFirstRow, expectedFirstRow)).toBeTruthy();

        var actualLastRow = game._getRow(9).cells;
        var expectedLastRow = [...game._getBlock(3, 1)._getRow(3), ...game._getBlock(3, 2)._getRow(3), ...game._getBlock(3, 3)._getRow(3)];
        expect(isArrayEqual(actualLastRow, expectedLastRow)).toBeTruthy();
    });

    it('should be able to get column', () => {
        var game = new Game();

        var actualFirstColumn = game._getColumn(1).cells;
        var expectedFirstColumn = [...game._getBlock(1, 1)._getColumn(1), ...game._getBlock(2, 1)._getColumn(1), ...game._getBlock(3, 1)._getColumn(1)];
        expect(isArrayEqual(actualFirstColumn, expectedFirstColumn)).toBeTruthy();

        var actualLastColumn = game._getColumn(9).cells;
        var expectedLastColumn = [...game._getBlock(1, 3)._getColumn(3), ...game._getBlock(2, 3)._getColumn(3), ...game._getBlock(3, 3)._getColumn(3)];
        expect(isArrayEqual(actualLastColumn, expectedLastColumn)).toBeTruthy();
    });

    it('should be initialized with rows model', () => {
        var game = new Game();

        expect(game.rows.length).toEqual(9);

        var isEveryRowCorrect = game.rows.every((row, index) => {
            var rowNum = index + 1;

            expect(row.cells.length).toEqual(9);

            var actualRow = row.cells;
            var expectedRow = game._getRow(rowNum).cells;

            return isArrayEqual(actualRow, expectedRow);
        });

        expect(isEveryRowCorrect).toBeTruthy();
    });

    it('should be initialized with columns model', () => {
        var game = new Game();

        expect(game.columns.length).toEqual(9);

        var isEveryColumnCorrect = game.columns.every((column, index) => {
            var columnNum = index + 1;

            expect(column.cells.length).toEqual(9);

            var actualColumn = column.cells;
            var expectedColumn = game._getColumn(columnNum).cells;

            return isArrayEqual(actualColumn, expectedColumn);
        });

        expect(isEveryColumnCorrect).toBeTruthy();
    });

    it('should be initialized with cells', () => {
        var game = new Game();

        expect(game.cells.length).toEqual(81);
    });

    it('should be able to be cloned', (done) => {
        var game = new Game();
        game.cells[0].resolve(2).then(() => {
            var game2 = game.clone();

            expect(game2.cells[0].isResolved()).toBeTruthy();
            expect(game2.cells[0].firstState()).toEqual(2);

            game2.cells[1].resolve(7).then(() => {
                expect(game2.cells[1].isResolved()).toBeTruthy();
                expect(game.cells[1].isResolved()).toBeFalsy();

                done();
            });
        });
    });
});

describe('Block resolution', () => {
    it('should receive block resolution event after all blocks are resolved', (done) => {
        var game = new Game();
        game.blocks.forEach(block => block._isResolved = true);
        game.blocks[0]._isResolved = false;

        game.blocks[0].resolve().then(() => {
            expect(game.isResolved()).toBeTruthy();

            done();
        });
    });
});

describe('Block invalidation', () => {
    it('should mark game as invalid after any block is invalidated', (done) => {
        var game = new Game();

        expect(game.isValid()).toBeTruthy();

        game.blocks[0].invalidate().then(() => {
            expect(game.isValid()).toBeFalsy();

            done();
        });
    });
});