import { Game } from '../../src/model/Game';
import { Cell } from '../../src/model/Cell';
import { Row } from '../../src/model/Row';
import { CellResolutionEvent } from '../../src/model/event/type/CellResolutionEvent';
import { CellReductionEvent } from '../../src/model/event/type/CellReductionEvent';
import {isSetEqual} from "./test-utils";

describe('Row model', () => {
    it('should receive cell resolution event after cell is resolved', () => {
        var game = new Game();
        var firstCell = game._getBlock(1, 1).cells[0];

        firstCell.resolve(7);

        var rowOne = game.rows[0];
        expect(rowOne._getEvents()[0] instanceof CellResolutionEvent).toBeTruthy();
    });

    it('should receive cell reduction event after cell is reduced', () => {
        var game = new Game();
        var firstCell = game._getBlock(1, 1).cells[0];

        firstCell.reduce(7);

        var rowOne = game.rows[0];
        expect(rowOne._getEvents()[0] instanceof CellReductionEvent).toBeTruthy();
    });

    it('should respond to cell resolution event and mutate states of children blocks', (done) => {
        var game = new Game();
        var firstCell = game._getBlock(1, 1).cells[0];

        var rowOne = game.rows[0];
        var expectedStates = new Set([1, 2, 3, 4, 5, 6, 8, 9]);
        firstCell.resolve(7).then(() => {
            expect(isSetEqual(rowOne.cells[0].states, new Set([7])));
            expect(isSetEqual(rowOne.cells[1].states, expectedStates));
            expect(isSetEqual(rowOne.cells[2].states, expectedStates));
            expect(isSetEqual(rowOne.cells[3].states, expectedStates));
            expect(isSetEqual(rowOne.cells[4].states, expectedStates));
            expect(isSetEqual(rowOne.cells[5].states, expectedStates));
            expect(isSetEqual(rowOne.cells[6].states, expectedStates));
            expect(isSetEqual(rowOne.cells[7].states, expectedStates));
            expect(isSetEqual(rowOne.cells[8].states, expectedStates));

            done();
        });
    });

    it('should respond to cell reduction event and mutate states of children blocks', (done) => {
        var createCellWith = (cell, targetSet) => {
            for(let state of cell.states) {
                if(!targetSet.has(state)) {
                    cell.states.delete(state);
                }
            }
            return cell;
        };

        var row = new Row([
            createCellWith(new Cell(), new Set([9])),
            createCellWith(new Cell(), new Set([6])),
            createCellWith(new Cell(), new Set([3, 4])),
            createCellWith(new Cell(), new Set([1, 4])),
            createCellWith(new Cell(), new Set([8])),
            createCellWith(new Cell(), new Set([1, 4, 7])),
            createCellWith(new Cell(), new Set([2, 5, 7])),
            createCellWith(new Cell(), new Set([2, 3, 7])),
            createCellWith(new Cell(), new Set([2, 3, 5, 7])),
        ]);

        row.cells[5].reduce(1).then(() => {
            expect(isSetEqual(row.cells[0].states, new Set([9])));
            expect(isSetEqual(row.cells[1].states, new Set([6])));
            expect(isSetEqual(row.cells[2].states, new Set([3, 4])));
            expect(isSetEqual(row.cells[3].states, new Set([1])));
            expect(isSetEqual(row.cells[4].states, new Set([8])));
            expect(isSetEqual(row.cells[5].states, new Set([4, 7])));
            expect(isSetEqual(row.cells[6].states, new Set([2, 5, 7])));
            expect(isSetEqual(row.cells[7].states, new Set([2, 3, 7])));
            expect(isSetEqual(row.cells[8].states, new Set([2, 3, 5, 7])));

            done();
        });
    });
});