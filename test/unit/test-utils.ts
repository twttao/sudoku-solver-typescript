const createCellWith = (cell, targetSet, isResolved = false) => {
    for(let state of cell.states) {
        if(!targetSet.has(state)) {
            cell.states.delete(state);
        }
    }

    cell._isResolved = isResolved;

    return cell;
};

const isSetEqual = (as, bs) => {
    if (as.size !== bs.size) {
        return false
    }

    for (var a of as) {
        if (!bs.has(a)) {
            return false;
        }
    }

    return true;
};

const isArrayEqual = (ar, br) => {
    if (ar.length !== br.length) {
        return false;
    }

    for(var a of ar) {
        if(!br.includes(a)) {
            return false;
        }
    }

    return true;
};

export { createCellWith, isSetEqual, isArrayEqual }