import { Cell } from '../../src/model/Cell';
import { isSetEqual } from './test-utils';
import {Game} from "../../src/model/Game";

describe('Cell model', () => {

    it('should be initialized with all possible states when created', () => {
        var cell = new Cell();

        expect(cell.isResolved()).toBeFalsy();

        var expectedState = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        expect(isSetEqual(cell.states, expectedState)).toBeTruthy();
    });

    it('should be able to resolve value', () => {
        var cell = new Cell();

        expect(cell.isResolved()).toBeFalsy();

        cell.resolve(8);

        expect(cell.isResolved()).toBeTruthy();
        var resolvedState = new Set([8]);
        expect(isSetEqual(cell.states, resolvedState)).toBeTruthy();
    });

    it('should be able to be cloned', (done) => {
        var cell = new Cell();
        cell.resolve(5).then(() => {
            var cell2 = cell.clone();
            expect(cell2.isResolved()).toBeTruthy();

            done();
        });
    });

    it('should report correct position in game', () => {
        var game = new Game();

        expect(game._getBlock(1, 1)._getRow(3)[2].positionInGame()).toEqual(8);

        expect(game._getBlock(1, 2)._getRow(3)[2].positionInGame()).toEqual(17);

        expect(game._getBlock(2, 1)._getRow(3)[2].positionInGame()).toEqual(35);
    });
});